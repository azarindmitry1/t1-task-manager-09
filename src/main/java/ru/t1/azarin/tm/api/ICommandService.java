package ru.t1.azarin.tm.api;

import ru.t1.azarin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
