package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.ICommandRepository;
import ru.t1.azarin.tm.constant.ArgumentConst;
import ru.t1.azarin.tm.constant.TerminalConst;
import ru.t1.azarin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display application version."
    );

    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display application command."
    );

    public static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display process and memory info."
    );

    public static Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS,
            "Display arguments of application."
    );

    public static Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS,
            "Display commands of application."
    );

    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            VERSION, ABOUT, HELP, INFO, ARGUMENTS, COMMANDS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
