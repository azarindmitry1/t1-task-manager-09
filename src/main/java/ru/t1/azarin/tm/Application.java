package ru.t1.azarin.tm;

import ru.t1.azarin.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
